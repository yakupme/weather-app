$(document).ready(function($) {
	
	$('.weather-container').weather({
		city: null,
		autocompleteMinLength: 3
	});

});

function FillChart(City,Hours) {
    $('.chart-container').highcharts({
        title: {
            text: 'City Temperature',
            x: -20 //center
        },
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
        xAxis: {
            categories: ['15:00', '18:00', '21:00', '00:00', '03:00', '06:00',
                '09:00', '12:00', '15:00', '18:00', '21:00']
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: City,
            data:Hours
        }
        ]
    });
};